import Vue from 'vue'
import Vuex from 'vuex'
import {categories} from './categories'
import {products} from './products'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    order: [],
    categories: categories,
    products: products,
  },
	getters: {
  		findCategory: (state) => (categoryId) =>{
  			return state.categories.find(eachCat => eachCat.id == categoryId)
		},
		findProductsInCategory: (state) => (categoryId) =>{
			return state.products.filter(eachProd => eachProd.category == categoryId)
		},
		findProduct: (state) => (productId) =>{
			return state.products.find(eachProd => eachProd.id == productId)
		}
	},
  mutations: {
	  addOrderItem(state, orderItem){
		  state.order.push(orderItem)
	  },
	  // deleteProduct(state, product){
		//   state.products = state.products.filter(eachProduct => eachProduct.id != product.id)
      //     delete state.productsByCategory[product.id]
	  // },
	  //
	  // createCategory(state, newCategory){
		//   state.products.push(newCategory)
	  // },
	  // deleteCategory(state, product){
		//   state.products = state.products.filter(eachProduct => eachProduct.id != product.id)
		//   delete state.productsByCategory[product.id]
	  // },
  },
  actions: {

  }
})
