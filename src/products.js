export const products = [
	{
		id: "foodel.category.Meat.Product1",
		category: "foodel.category.Meat",
		title: "Мясо1",
		description: {
			title: "Мясо1",
			content: "blah blah blah"
		},
		weight: "500g",
		price: "12.50",
		icon: "meat"
	},
	{
		id: "foodel.category.Meat.Product2",
		category: "foodel.category.Meat",
		title: "Мясо2",
		description: {
			title: "Мясо2",
			content: "blah blah blah"
		},
		weight: "600g",
		price: "12.50",
		icon: "meat"
	},
	{
		id: "foodel.category.Meat.Product3",
		category: "foodel.category.Meat",
		title: "Мясо3",
		description: {
			title: "Мясо3",
			content: "blah blah blah"
		},
		weight: "400g",
		price: "12.50",
		icon: "meat"
	},
	{
		id: "foodel.category.Dairy.Product1",
		category: "foodel.category.Dairy",
		title: "Молоко 1324524352435",
		description: {
			title: "Молоко 1324524352435",
			content: "blah blah blah"
		},
		weight: "500g",
		price: "34.50",
		icon: "milk-bottle"
	},
	{
		id: "foodel.category.Dairy.Product2",
		category: "foodel.category.Dairy",
		title: "Молоко 2",
		description: {
			title: "Молоко 2",
			content: "blah blah blah"
		},
		weight: "600g",
		price: "55.50",
		icon: "milk-bottle"
	},
	{
		id: "foodel.category.Dairy.Product3",
		category: "foodel.category.Dairy",
		title: "Молоко 3",
		description: {
			title: "Молоко 3",
			content: "blah blah blah"
		},
		weight: "400g",
		price: "12.50",
		icon: "milk-bottle"
	},
	{
		id: "foodel.category.Dairy.Product4",
		category: "foodel.category.Dairy",
		title: "Молоко 4",
		description: {
			title: "Молоко 4",
			content: "blah blah blah"
		},
		weight: "600g",
		price: "55.50",
		icon: "milk-bottle"
	},
	{
		id: "foodel.category.Dairy.Product5",
		category: "foodel.category.Dairy",
		title: "Молоко 5",
		description: {
			title: "Молоко 5",
			content: "blah blah blah"
		},
		weight: "400g",
		price: "12.50",
		icon: "milk-bottle"
	},
	{
		id: "foodel.category.Drinks.Product1",
		category: "foodel.category.Drinks",
		title: "Напитки 1",
		weight: "2L",
		price: "12.50",
		icon: "water"
	},
	{
		id: "foodel.category.Drinks.Product2",
		category: "foodel.category.Drinks",
		title: "Напитки 2",
		weight: "1L",
		price: "150.00",
		icon: "water"
	},
	{
		id: "foodel.category.Drinks.Product3",
		category: "foodel.category.Drinks",
		title: "Напитки 3",
		weight: "2L",
		price: "1.30",
		icon: "water"
	},
	{
		id: "foodel.category.FastFood.Product1",
		category: "foodel.category.FastFood",
		title: "Деликатессы 1",
		weight: "100g",
		price: "7.50",
		icon: "burger"
	},
	{
		id: "foodel.category.FastFood.Product2",
		category: "foodel.category.FastFood",
		title: "Деликатессы 2",
		weight: "500g",
		price: "12.50",
		icon: "burger"
	},
	{
		id: "foodel.category.FastFood.Product3",
		category: "foodel.category.FastFood",
		title: "Деликатессы 3",
		weight: "200g",
		price: "1.50",
		icon: "burger"
	},

]