export const categories = [
	{
		id: "foodel.category.Meat",
		title: "Мясо",
		icon: "meat"
	},
	{
		id: "foodel.category.Dairy",
		title: "Молоко",
		icon: "milk-bottle"
	},
	{
		id: "foodel.category.Drinks",
		title: "Напитки",
		icon: "water"
	},
	{
		id: "foodel.category.FastFood",
		title: "Деликатессы",
		icon: "burger"
	},


	{
		id: "foodel.category.Meat222",
		title: "Meat222",
		icon: "meat"
	},
	{
		id: "foodel.category.Dairy222",
		title: "Dairy222",
		icon: "milk-bottle"
	},
	{
		id: "foodel.category.Drinks222",
		title: "Drinks222",
		icon: "water"
	},
	{
		id: "foodel.category.FastFood222",
		title: "FastFood222",
		icon: "burger"
	},
]