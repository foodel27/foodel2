module.exports = {
  pluginOptions: {
    moment: {
      locales: [
        'en',
        'ru',
        'he'
      ]
    }
  }
}
